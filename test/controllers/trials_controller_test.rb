require 'test_helper'

class TrialsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get trials_new_url
    assert_response :success
  end

  test "should get show" do
    get trials_show_url
    assert_response :success
  end

  test "should get index" do
    get trials_index_url
    assert_response :success
  end

end
