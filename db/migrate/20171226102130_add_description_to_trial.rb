class AddDescriptionToTrial < ActiveRecord::Migration[5.1]
  def change
    add_column :trials, :description, :text
  end
end
