class CreateTrials < ActiveRecord::Migration[5.1]
  def change
    create_table :trials do |t|
      t.string :title
      t.string :status
      t.string :tag
      t.datetime :date_proposed
      t.datetime :enrollment_start
      t.datetime :enrollment_end

      t.timestamps
    end
  end
end
