# Randomized Trials Sample

You can visit the [*live version here*](https://randomized-trials-sample.herokuapp.com/)

or you can play with the app by cloning the repo and then installing the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ bin/rails db:migrate
```

Now you're ready to run the app in a local server:

```
$ bin/rails server
```