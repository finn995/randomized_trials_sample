class User < ApplicationRecord
	
	before_save { self.email = email.downcase }
	
	before_save { self.phone = phone.to_i }
	
	validates :name,		 presence: true, length: { minimium: 2, maximum: 15 }
	validates :phone,		 presence: true
	
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :email, 	 presence: true, length: { maximum: 255 },
																																 uniqueness: { case_sensitive: false },
																																 format: { with: VALID_EMAIL_REGEX }
	
	validates :password, presence: true, length: { minimum: 6 }
	has_secure_password
end