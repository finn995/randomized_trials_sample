class TrialsController < ApplicationController
  
  def new
    @trial = Trial.new
  end
  
  def create
    @trial = Trial.new(trial_params)
    if @trial.save
      flash[:success] = "Trial created successfully"
      redirect_to trials_path
    else
      render 'new'
    end
  end

  def show
    @trial = Trial.find(params[:id])
  end

  def index
    @trials = Trial.all
  end
  
  private
  
    def trial_params
      params.require(:trial).permit(:title, :description, :enrollment_start, :enrollment_end)
    end
  
end