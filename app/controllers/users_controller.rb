class UsersController < ApplicationController
  before_action :set_user,       only:   [:show, :edit, :update, :destroy]
  before_action :logged_in_user, except: [:new, :create]
  before_action :correct_user,   only:   [:edit, :update, :destroy]
  before_action :admin_user,     only:   [:destroy]
  
  def index
    @users = User.paginate(page: params[:page])
  end

  def show
  end

  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = 'You created a new account'
      log_in @user
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = 'Account updated'
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    @user.destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  private
  
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :phone, :first_name, :last_name)
    end
    
    def set_user
      @user = User.find(params[:id])
    end
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
  
end